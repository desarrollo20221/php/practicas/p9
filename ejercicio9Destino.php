<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>       
        <?php
        $nota = $_GET["nota"];
        $salida = "";
        if ($nota <= 3) {
            $salida = "Deficiente";
        } elseif ($nota < 5) {
            $salida = "Supenso";
        } elseif ($nota <= 6) {
            $salida = "Suficiente";
        } elseif ($nota < 7) {
            $salida = "Bien";
        } elseif ($nota < 9) {
            $salida = "Notable";
        } else {
            $salida = "Sobresaliente";
        }


        echo $salida;
        ?>
    </body>
</html>
